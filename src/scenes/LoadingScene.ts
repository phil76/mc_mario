import { Scene } from 'phaser';
import { FONT, FONT_SIZE, HEIGHT, SCENE_NAME, WIDTH } from '../constant/config';

import marioAtlas from '../assets/graphics/mario-atlas.png';
import marioAtlasBig from '../assets/graphics/mario-big-atlas.png';
import tiles from '../assets/graphics/tiles.png';
import map1 from '../assets/map/world1-1.json';
import map2 from '../assets/map/world1-2.json';
import dieSfx from '../assets/sfx/Die.wav';
import jumpSfx from '../assets/sfx/Jump.wav';
import kickSfx from '../assets/sfx/Kick.wav';
import musicSfx from '../assets/sfx/SMBOverworldDiskWriter.ogg';
import endStageSfx from '../assets/sfx/end-stage.wav';
import Under_Ground_Theme_SMB from '../assets/sfx/smb-underground.ogg';
import powerupSfx from '../assets/sfx/Powerup.wav';
import itemSfx from '../assets/sfx/Item.wav';
import brickParticle from '../assets/graphics/brickParticle.png';


/**
 * @description a loading scene example, handle the preload of all assets
 * @author © Philippe Pereira 2020
 * @export
 * @class LoadingScene
 * @extends {Scene}
 */
export default class LoadingScene extends Scene
{
    constructor()
    {
        super({
            key: SCENE_NAME.LOADING as string,
            //  Splash screen and progress bar textures.
            pack: {
                files: [{
                    key: 'background',
                    type: 'image'
                }, {
                    key: 'logo',
                    type: 'image'
                }, {
                    key: 'progressBar',
                    type: 'image'
                },{
                    key: 'progressBarBg',
                    type: 'image'
                }]
            } as any
        });
    }

    public preload(): void
    {
        //  Display cover and progress bar textures.
        this.showCover();
        this.showProgressBar();

        // Preload all assets here, ex:
        this.load.image('tiles', tiles);
        this.load.tilemapTiledJSON('map1', map1);
        this.load.tilemapTiledJSON('map2', map2);
        this.load.spritesheet('mario', marioAtlas, { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('marioBig', marioAtlasBig, { frameWidth: 20, frameHeight: 32 });
        this.load.audio('dieSfx', dieSfx);
        this.load.audio('jumpSfx', jumpSfx);
        this.load.audio('kickSfx', kickSfx);
        this.load.audio('map1MusicSfx', musicSfx);
        this.load.audio('endStageSfx', endStageSfx);
        this.load.audio('map2MusicSfx', Under_Ground_Theme_SMB);
        this.load.audio('powerupSfx', powerupSfx);
        this.load.audio('itemSfx', itemSfx);
        this.load.image('brickParticle', brickParticle);
    }

    /**
     * Display cover
     */
    private showCover(): void
    {
        this.cameras.main.setRoundPixels(true);

        this.add.image(WIDTH / 2, HEIGHT / 2, 'background');
        this.add.image(WIDTH / 2, HEIGHT / 3, 'logo');

        // progress bar white background
        this.add.image(WIDTH / 2, HEIGHT / 4 * 3 + 2, 'progressBarBg')
            .setDisplaySize(WIDTH / 4 * 3, 6)
            .setVisible(true);
    }

    /**
     * Display progress bar and percentage text
     */
    private showProgressBar(): void
    {
        //  Get the progress bar filler texture dimensions.
        const { width: w, height: h } = this.textures.get('progressBar').get();

        //  Place the filler over the progress bar of the splash screen.
        const img: Phaser.GameObjects.Sprite = this.add.sprite(WIDTH / 2, HEIGHT / 4 * 3, 'progressBar')
            .setOrigin(0.5, 0)
            .setDisplaySize(WIDTH / 4 * 3 - 2, 4);

        // Add percentage text
        const loadingpercentage: Phaser.GameObjects.BitmapText = this.add.bitmapText(WIDTH / 2, HEIGHT / 3 * 2, FONT, 'loading:', FONT_SIZE, 1)
            .setOrigin(0.5, 0.5)
            .setAlpha(1);

        //  Crop the filler along its width, proportional to the amount of files loaded.
        this.load.on('progress', (progress: number) =>
        {
            loadingpercentage.text = `loading: ${Math.round(progress * 100)}%`;

            img.setCrop(0, 0, Math.ceil(progress * w), h);

        }).on('complete', () =>
        {
            loadingpercentage.text = 'press any key to start';

            this.startGameScene();
        });
    }

    private startGameScene(): void
    {
        this.input.keyboard.once('keydown', () =>
        {
            this.scene.start(SCENE_NAME.GAME);
        });
    }
}