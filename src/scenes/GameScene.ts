import { Scene } from 'phaser';
import { WIDTH, HEIGHT, FONT, FONT_SIZE, SCENE_NAME } from '../constant/config';
import Coin from '../gameObjects/Coin';
import Door from '../gameObjects/Door';
import Goomba from '../gameObjects/Goomba';
import Mario from '../gameObjects/Mario';
import Mushroom from '../gameObjects/Mushroom';

/**
 * @description a main game scene example
 * @author © Philippe Pereira 2020
 * @export
 * @class GameScene
 * @extends {Scene}
 */
export default class GameScene extends Scene
{
    public map: Phaser.Tilemaps.Tilemap;
    public currentMapName: string = 'map1';
    private tileset: string | Phaser.Tilemaps.Tileset | string[] | Phaser.Tilemaps.Tileset[];
    private platformLayer: Phaser.Tilemaps.TilemapLayer;
    private backgroundLayer: Phaser.Tilemaps.TilemapLayer;
    private mario: Mario;
    public marioLife: number = 3;
    debugGraphics: Phaser.GameObjects.Graphics;
    public goombas: Goomba[] = [];
    public coins: Coin[] = [];
    public doors: Door[] = [];
    public music: Phaser.Sound.BaseSound;
    private endStageMusic: Phaser.Sound.BaseSound;
    constructor()
    {
        super({ key: SCENE_NAME.GAME as string });
    }

    public init(): void
    {
        // reset life, needed after game over
        //this.mario?.coinCount = 0;

        // fading the scene from black
        this.cameras.main.fadeIn(500);
    }


    public create(): void
    {
        // set the fps to 120 for good collisions at high speed (only if needed)
        this.physics.world.setFPS(120);

        // add the tiled map
        this.map = this.make.tilemap({ key: this.currentMapName });

        // add the tileset associated to the map
        this.tileset = this.map.addTilesetImage('tiles', 'tiles');

        // add the tiled map layers
        this.addLayers();

        // add mario
        this.mario = new Mario(this, 32, HEIGHT - 16, 'mario', 0);

        // add enemies, coins, doors, etc from map objects layer
        this.addObjectsFromMap();

        // add colliders
        this.addColliders();

        // handle the camera
        this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels)
            .startFollow(this.mario);

        // Launch the HUD Scene
        this.scene.launch(SCENE_NAME.HUD).setActive(true, SCENE_NAME.HUD);

        // the music...
        this.handleMusic();
        this.endStageMusic = this.sound.add('endStageSfx', { volume: 0.4 });

        // tiled debug
        // this.debugGraphics = this.add.graphics().setDepth(4000);

        // this.platform.renderDebug(this.debugGraphics, {
        //     tileColor: null, // Non-colliding tiles
        //     collidingTileColor: null, // new Phaser.Display.Color(243, 134, 48, 50), // Colliding tiles
        //     faceColor: new Phaser.Display.Color(227, 6, 6, 255) // Colliding face edges
        // });
    }

    public update(time: number, delta: number): void
    {
        // Handle logic here
    }

    /**
     * Add the Tiled layers
     */
    private addLayers(): void
    {
        this.backgroundLayer = this.map.createLayer('background', this.tileset, 0, 0).setDepth(0);

        this.platformLayer = this.map.createLayer('platform', this.tileset, 0, 0).setDepth(10);
    }

    /**
     * Convert Tiled objects layer into Sprites
     */
    private addObjectsFromMap(): void
    {
        this.map.createFromObjects('objects', [
            // @ts-ignore
            { name: 'coin', classType: Coin, key: 'mario', frame: 6 },
            // @ts-ignore
            { name: 'goomba', classType: Goomba, key: 'mario', frame: 11 },
            // @ts-ignore
            { name: 'door', classType: Door, key: 'mario', frame: 15 },
        ]);
    }

    /**
     * Add all needed colliders
     */
    private addColliders(): void
    {
        // add the collision tiles
        this.platformLayer.setCollisionByProperty({ collides: true });

        this.physics.world.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
        this.physics.add.collider(this.mario, this.platformLayer, this.handleCollideTiles, undefined, this);
        this.physics.add.collider(this.goombas, this.platformLayer, undefined, undefined, this);
        this.physics.add.collider(this.mario, this.goombas, this.handleCollideGoombas, undefined, this);
        this.physics.add.overlap(this.mario, this.coins, this.handleCollideCoins, undefined, this);
        this.physics.add.overlap(this.mario, this.doors, this.handleExit, undefined, this);
    }

    private handleCollideTiles(_player, _tile): void
    {
        const mario: Mario = _player;
        const tile: Phaser.Tilemaps.Tile = _tile;

        if (tile.properties.mushroom && mario.body.blocked.up)
        {
            tile.properties.mushroom = false;
            const mushroom = new Mushroom(this, tile.pixelX + 8, tile.pixelY, 'mario', 17);
            this.physics.add.collider(mushroom, this.platformLayer, undefined, undefined, this);
            this.physics.add.overlap(this.mario, mushroom, this.mario.getPowerUp, undefined, this.mario);
        }

        if (tile.properties.destructible && mario.body.blocked.up)
        {
            tile.properties.destructible = false;
            tile.properties.collides = false;
            

            const bounds: Phaser.Geom.Rectangle = tile.getBounds() as unknown as Phaser.Geom.Rectangle;

            console.log(tile)


            const particles = this.add.particles('brickParticle');

            const emitter = particles.createEmitter({});

            // const emitter = particles.createEmitter({
            //     //frame: 'missileTrail',
            //     //blendMode: 'ADD',
            //     frequency: -1,
            //     quantity: 4,
            //     speed: 200,
            //     lifespan: 975,
            //     // rotate: {
            //     //     onEmit: (particle) =>
            //     //     {
            //     //         // @ts-ignore
            //     //         return particle.emitter.follow.angle;
            //     //     }
            //     // },
            //     on: false,
            // });

            //emitter.start()
            this.platformLayer.removeTileAt(tile.x, tile.y);

            emitter.setFrequency(-1).setQuantity(1).setLifespan(300).setSpeedX(64).setSpeedY(64).setRadial(true).emitParticleAt(bounds.centerX, bounds.centerY);
            emitter.setFrequency(-1).setQuantity(1).setLifespan(300).setSpeedX(64).setSpeedY(-64).setRadial(true).emitParticleAt(bounds.centerX, bounds.centerY);
            emitter.setFrequency(-1).setQuantity(1).setLifespan(300).setSpeedX(-64).setSpeedY(-64).setRadial(true).emitParticleAt(bounds.centerX, bounds.centerY);
            emitter.setFrequency(-1).setQuantity(1).setLifespan(300).setSpeedX(-64).setSpeedY(64).setRadial(true).emitParticleAt(bounds.centerX, bounds.centerY);
            
        }


    }

    /**
     * Handle collision between mario and enemies
     * @param player 
     * @param enemy 
     */
    private handleCollideGoombas(player, enemy): void
    {
        const mario: Mario = player;
        const goomba: Goomba = enemy;

        if (goomba?.body.touching.left || goomba?.body.touching.right)
        {
            goomba?.body.stop();
            mario?.die();
            return;
        }

        if (mario?.body.touching.down && goomba?.body.touching.up)
        {
            goomba.die();
            mario.body.setVelocityY(-mario.body.speed / 2);
        }
    }

    /**
     * Handle collision between mario and coins
     * @param _player 
     * @param _coin 
     */
    private handleCollideCoins(_player: Phaser.GameObjects.GameObject, _coin: Phaser.GameObjects.GameObject)
    {
        const coin = _coin as unknown as Coin;
        const player = _player as unknown as Mario;

        coin.body.setEnable(false);
        coin.setActive(false).setVisible(false);

        player.setCoinCount();
        this.sound.play('coinSfx');
    }

    /**
     * Change music according to stage name, and play it
     */
    handleMusic(): void
    {
        this.music = this.sound.add(`${this.currentMapName}MusicSfx`, { volume: 0.4, loop: true });
        this.music.play();
    }

    /**
     * Handle map change when mario hit a door
     */
    private handleExit(_player: Phaser.GameObjects.GameObject, _door: Phaser.GameObjects.GameObject)
    {
        const player = _player as unknown as Mario;
        const door = _door as unknown as Door;

        // stop mario
        player.body.stop().setEnable(false);
        player.pause();
        player.anims.play('idle');

        // add a tween to make slowly disappear mario after a 4s delay
        this.add.tween({
            targets: player,
            duration: 1000,
            delay: 4000,
            alpha: 0,
        })

        // stop stage music
        this.music.stop();

        // start end stage music
        this.endStageMusic.play();

        // start next stage when the endStageMusic ends
        this.endStageMusic.once('complete', () =>
        {
            player.alpha = 1;
            this.cameras.main.fadeOut(500)
            // destroy current map and gameObjects
            this.physics.world.colliders.destroy();

            this.map.destroy();
            this.coins.forEach(coin => coin.destroy());
            this.goombas.forEach(goomba => goomba.destroy());
            this.doors.forEach(door => door.destroy());

            // create the new map
            this.currentMapName = door.name;
            this.map = this.make.tilemap({ key: door.name });
            this.addLayers();
            this.addObjectsFromMap();
            this.addColliders();
            this.cameras.main.fadeIn(500);

            // change mario position
            this.mario.body.setEnable(true).reset(32, HEIGHT - 16);
            // unpause mario
            player.pause();

            // start the new stage music
            this.handleMusic();
        });
    }

    /**
     * start game over scene when mario die
     */
    public gameOver(): void
    {
        this.physics.add.collider(this.mario, this.goombas, undefined, undefined, this);
        this.physics.add.overlap(this.mario, this.coins, undefined, undefined, this);

        this.time.addEvent({
            delay: 1000,
            callback: () =>
            {
                this.scene.start(SCENE_NAME.GAME_OVER);
            }
        })
    }
}
