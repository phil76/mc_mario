import GameScene from "../scenes/GameScene";

export default class Mario extends Phaser.GameObjects.Sprite
{
    public scene: GameScene;
    public body: Phaser.Physics.Arcade.Body;
    private keys: Phaser.Types.Input.Keyboard.CursorKeys;
    private jumpTime: number = 0;
    private speed: number = 100;
    private runSpeed: number = 1;
    private isDead: boolean = false;
    private coinCount: number = 0;
    private isPaused: boolean = false;
    private isBig: string = '';
    private hitMomentum: boolean = false;

    constructor(scene: GameScene, x: number, y: number, texture: string, frame: number)
    {
        super(scene, x, y, texture, frame);

        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        this.body.setCollideWorldBounds(true).setSize(12, 16, true);

        this.keys = this.scene.input.keyboard.createCursorKeys();

        this.setDepth(10).createAnims();
    }

    public preUpdate(time: number, delta: number): void
    {
        super.preUpdate(time, delta);

        if (this.isDead || this.isPaused)
        {
            return;
        }

        const { left, right, up, down, shift } = this.keys;

        // handle walk direction
        if (left.isDown && this.body.blocked.down)
        {
            this.setFlipX(true);

            this.body.setVelocityX(-this.speed * this.runSpeed);

            this.anims.play(`run${this.isBig}`, true);
        }
        else if (right.isDown && this.body.blocked.down)
        {
            this.setFlipX(false);

            this.body.setVelocityX(this.speed * this.runSpeed);

            this.anims.play(`run${this.isBig}`, true);
        }
        else if (right.isUp && left.isUp && this.body.blocked.down)
        {
            this.body.setVelocityX(0);

            this.anims.play(`idle${this.isBig}`);
        }

        // handle air turn control
        if (left.isDown && this.body.velocity.x >= 0 && !this.body.blocked.down)
        {
            this.setFlipX(true);

            this.body.setVelocityX(-this.speed / 4 * 3 * this.runSpeed);
        }
        else if (right.isDown && this.body.velocity.x <= 0 && !this.body.blocked.down)
        {
            this.setFlipX(false);

            this.body.setVelocityX(this.speed / 4 * 3 * this.runSpeed);
        }

        // handle jump
        if (up.isDown && this.body.blocked.down && up.getDuration() < 250 && this.jumpTime < time + 200)
        {
            this.jumpTime = time;

            this.scene.sound.play('jumpSfx');

            if (shift.isUp)
            {
                this.body.setVelocityY(-2 * this.speed / 1.1);
            }
            else
            {
                this.body.setVelocityY(-2 * this.speed / 1.8 * this.runSpeed);
            }
            
            this.anims.play(`jump${this.isBig}`);
        }

        // handle run
        if (shift.isDown && this.body.blocked.down)
        {
            this.runSpeed = 1.5;
        }
        else if (shift.isUp && this.body.blocked.down)
        {
            this.runSpeed = 1;
        }

        // if pressing down while big
        if (down.isDown && left.isUp && right.isUp && this.isBig === 'Big')
        {
            this.anims.play('crouchBig', true);
        }

        // fall in water or void
        if (this.y + this.height > this.scene.map.heightInPixels + 16)
        {
            this.die();
        }
    }

    public getPowerUp(mario, shroom)
    {
        shroom.die();

        this.scene.physics.add.overlap(this, shroom, undefined, undefined, this);
        
        this.setBig();
    }

    private setBig(): void
    {
        this.isBig = 'Big';

        this.scene.sound.play('powerupSfx');

        this.body.setSize(16, 30, true).setOffset(2, 1);
    }

    private setLittle(): void
    {
        this.isBig = '';

        this.scene.sound.play('powerupSfx');

        this.body.setSize(12, 16, true).setOffset(2, 0);
    }

    private setHitMomentum(): void
    {
        this.hitMomentum = true;

        this.scene.time.addEvent({
            delay: 1000,
            callback: () => {
                this.hitMomentum = false;
            }
        })
    }

    /**
     * Create the mario animations
     */
    private createAnims(): void
    {
        this.scene.anims.create({
            key: 'idle',
            frames: this.anims.generateFrameNumbers('mario', { frames: [0] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'run',
            frames: this.anims.generateFrameNumbers('mario', { frames: [1, 2, 3] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'jump',
            frames: this.anims.generateFrameNumbers('mario', { frames: [4] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'dead',
            frames: this.anims.generateFrameNumbers('mario', { frames: [5] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'idleBig',
            frames: this.anims.generateFrameNumbers('marioBig', { frames: [0] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'runBig',
            frames: this.anims.generateFrameNumbers('marioBig', { frames: [1, 2, 3] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'jumpBig',
            frames: this.anims.generateFrameNumbers('marioBig', { frames: [4] }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'crouchBig',
            frames: this.anims.generateFrameNumbers('marioBig', { frames: [5] }),
            frameRate: 10,
            repeat: -1
        });
    }

    /**
     * Keep coin count
     */
    public setCoinCount()
    {
        this.coinCount += 1;

        this.scene.events.emit('setCoin', this.coinCount);
    }

    /**
     * toggle mario pause
     */
    public pause()
    {
        this.isPaused = !this.isPaused;
    }

    /**
     * Handle mario's death
     */
    public die(): void
    {
        if (this.isBig === 'Big')
        {
            this.setLittle();
            this.setHitMomentum();
            return;
        }

        if (this.isDead || this.hitMomentum)
        {
            return;
        }

        this.isDead = true;

        this.scene.marioLife -= 1;

        if (this.scene.marioLife < 0)
        {
            this.scene.marioLife = 3;
            this.scene.currentMapName = 'map1';
        }

        this.scene.music.stop();

        this.scene.sound.play('dieSfx');

        this.body.stop().setAllowGravity(false);

        this.anims.play('dead');

        this.scene.gameOver();
    }
}