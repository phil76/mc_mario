import GameScene from "../scenes/GameScene";

export default class Goomba extends Phaser.GameObjects.Sprite
{
    public scene: GameScene;
    public body: Phaser.Physics.Arcade.Body;
    private isDead: boolean;
    private speed: number = 30;

    constructor(scene: GameScene, x: number, y: number, texture: string, frame: number)
    {
        super(scene, x, y, texture, frame);

        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        this.body.setCollideWorldBounds(true)
            .setSize(16, 16, false)
            .setVelocityX(-this.speed)
            .setBounce(0.2, 0.2);

        this.setDepth(20);

        this.createAnims();

        this.anims.play('goomba_walk', true);

        this.scene.goombas.push(this);
    }

    public preUpdate(time: number, delta: number): void
    {
        super.preUpdate(time, delta);

        if (this.isDead)
        {
            return;
        }

        // handle walk direction
        if (this.body.blocked.right)
        {
            this.setFlipX(true);

            this.body.setVelocityX(-this.speed);
        }
        else if (this.body.blocked.left)
        {
            this.setFlipX(false);

            this.body.setVelocityX(this.speed);
        }

        if (this.body.velocity.x === 0)
        {
            this.body.setVelocityX(this.speed);
        }

        // fall in water or void
        if (this.y + this.height > this.scene.map.heightInPixels)
        {
            this.die();
        }
    }

    /**
     * Create the goomba animations
     */
    private createAnims(): void
    {
        this.scene.anims.create({
            key: 'goomba_walk',
            frames: this.anims.generateFrameNumbers('mario', { frames: [11, 12] }),
            frameRate: 8,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'goomba_dead',
            frames: this.anims.generateFrameNumbers('mario', { frames: [10, 14] }),
            frameRate: 3,
            repeat: 0
        });
    }

    public die(): void
    {
        this.isDead = true;

        this.body.stop();

        this.anims.play('goomba_dead');

        this.scene.sound.play('kickSfx');

        this.body.setEnable(false);
    }
}