import GameScene from "../scenes/GameScene";

export default class Mushroom extends Phaser.GameObjects.Sprite
{
    public scene: GameScene;
    public body: Phaser.Physics.Arcade.Body;
    private isDead: boolean;
    private speed: number = 50;
    private isReady: boolean = false;

    constructor(scene: GameScene, x: number, y: number, texture: string, frame: number)
    {
        super(scene, x, y, texture, frame);

        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        this.body.setSize(16, 16, false)
            .setVelocityX(this.speed)
            .setEnable(false);

        this.setDepth(5);
        this.growUp();
    }

    public preUpdate(time: number, delta: number): void
    {
        super.preUpdate(time, delta);

        if (this.isDead || !this.isReady)
        {
            return;
        }

        // handle walk direction
        if (this.body.blocked.right)
        {
            this.body.setVelocityX(-this.speed);
        }
        else if (this.body.blocked.left)
        {
            this.body.setVelocityX(this.speed);
        }

        // fall in water or void
        if (this.y + this.height > this.scene.map.heightInPixels)
        {
            this.die();
        }
    }

    public growUp(): void
    {
        this.scene.sound.play('itemSfx');

        this.scene.add.tween({
            targets: this,
            y: this.y - 16,
            duration: 500,
            onComplete: () => {
                this.body.setEnable(true);
                this.isReady = true;
            }
        })
    }

    public die(): void
    {
        this.isDead = true;

        this.body.stop();

        this.body.setEnable(false);

        this.destroy();
    }
}