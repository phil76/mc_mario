import GameScene from "../scenes/GameScene";

export default class Coin extends Phaser.GameObjects.Sprite
{
    public scene: GameScene;
    public body: Phaser.Physics.Arcade.Body;
    private speed: number = 30;

    constructor(scene: GameScene, x: number, y: number)
    {
        super(scene, x, y, '', undefined);

        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        this.body.setImmovable()
            .setAllowGravity(false)
            .setSize(16, 16, false);

        this.setDepth(5);
        this.createAnims();
        this.anims.play('coin', true);
        this.scene.coins.push(this);
    }

    /**
     * Create the coin animations
     */
    private createAnims(): void
    {
        this.scene.anims.create({
            key: 'coin',
            frames: this.anims.generateFrameNumbers('mario', { frames: [6, 7, 8, 9] }),
            frameRate: 8,
            repeat: -1
        });
    }
}